﻿using System;
using System.Collections.Generic;
using IMDB_App.Domain;

namespace IMDB_App.Domain
{
    public class Movie
    {
        public string Name { get; set; }

        public DateTime ReleaseDate { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors;
        public Producer Producer;

        public Movie()
        {

        }
        public Movie(string moviesName, DateTime moviesReleaseDate, String moviesPlot, List<Actor> actorList, Producer producer)
        {
            Name = moviesName;
            ReleaseDate = moviesReleaseDate;
            Plot = moviesPlot;
            Actors = actorList;
            Producer = producer;
        }
       
       
    }
}
