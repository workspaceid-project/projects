﻿using System;

namespace IMDB_App.Domain
{
    public class Producer
    {
        public string Name { get; set; }
        

        public DateTime Dob { get; set; }
        public Producer()
        {

        }
        public Producer(string producerName, DateTime producerDob)
        {
            Name = producerName;
            Dob = producerDob;

        }
       
    }
}
