﻿using System;

namespace IMDB_App.Domain
{
    public class Actor
    {
        public string Name { get; set; }

        public DateTime Dob { get; set; }
        public Actor()
        {

        }
        public Actor(string actorName, DateTime actorDob)
        {
            Name = actorName;
            Dob = actorDob;
        }
        

        
    }
}
