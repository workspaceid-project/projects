﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using IMDB_App.Repository;
using IMDB_App.Domain;
using System.Linq;

namespace IMDB_App
{
    public class SampleData
    {
        public void SampleDataForActor()
        {
            List<Actor> actors = new List<Actor>()
            {
                new Actor(){Name="salman",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="sahrukh",Dob=new DateTime(1969,5,12)},
                new Actor(){Name="akshay",Dob=new DateTime(1968,12,11)},
                new Actor(){Name="salman",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="ajay",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="ranveer",Dob=new DateTime(1988,11,2)},
                new Actor(){Name="shahid",Dob=new DateTime(1987,10,3)}

            };
        }
        public void SampleDataForProducer()
        {
            List<Producer> producers = new List<Producer>()
            {
                new Producer(){Name="salman",Dob=new DateTime(1967,12,12)},
                new Producer(){Name="sahrukh",Dob=new DateTime(1969,5,12)},
                new Producer(){Name="akshay",Dob=new DateTime(1968,12,11)},
                new Producer(){Name=" priyanka chopra",Dob=new DateTime(1977,2,13)},
                new Producer(){Name="deepika padukone",Dob=new DateTime(1976,4,12)},
                new Producer(){Name="siddharth roy",Dob=new DateTime(1967,12,12)},
                new Producer(){Name="mahesh bhatt",Dob=new DateTime(1955,12,12)}

            };
        }
        public void SampleDataForMovies()
        {
            List<Actor> actors1 = new List<Actor>()
            {
                new Actor(){Name="salman",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="randeep hooda",Dob=new DateTime(1968,12,11)}
            };
            List<Actor> actors2 = new List<Actor>()
            {
                new Actor(){Name="akshay",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="Aniruddh Dave",Dob=new DateTime(1985,5,12)}
                
            };
            List<Actor> actors3 = new List<Actor>()
            {
                new Actor(){Name="abhishek bachchan",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="uday chopra",Dob=new DateTime(1985,5,12)},
                new Actor(){Name="john abraham",Dob=new DateTime(1968,12,11)}
            };
            List<Actor> actors4 = new List<Actor>()
            {
                new Actor(){Name="akshay",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="nana patekar",Dob=new DateTime(1985,5,12)},
                new Actor(){Name="feroz khan",Dob=new DateTime(1968,12,11)}
            };
            List<Actor> actors5 = new List<Actor>()
            {
                new Actor(){Name="ajay",Dob=new DateTime(1967,12,12)},
                new Actor(){Name="ammy virk",Dob=new DateTime(1985,5,12)},
                new Actor(){Name="sanjay datt",Dob=new DateTime(1968,12,11)}
            };

            List<Actor> actors6 = new List<Actor>()
            {
                new Actor(){Name="shahid kapoor",Dob=new DateTime(1977,12,5)},
                new Actor(){Name="kiara advani",Dob=new DateTime(1985,5,12)}
                
            };
            List<Actor> actors7 = new List<Actor>()
            {
                new Actor(){Name="akshay",Dob=new DateTime(1968,12,11)},
                new Actor(){Name="vidya balan",Dob=new DateTime(1985,5,12)},
                new Actor(){Name="sharman joshi",Dob=new DateTime(1968,12,11)}
            };

            List<Movie> movies = new List<Movie>()
            {
                new Movie(){Name="radhe",ReleaseDate=new DateTime(2021,5,12),Plot="this is a action movie",Actors=actors1,Producer=new Producer(){Name="salman",Dob=new DateTime(1967,12,12)} },
                new Movie(){Name="bellbottom",ReleaseDate=new DateTime(1967,12,12),Plot="awesome movie",Actors=actors2,Producer=new Producer(){Name="akshay",Dob=new DateTime(1968,12,11)} },
                new Movie(){Name="dhoom",ReleaseDate=new DateTime(1967,12,12),Plot="action thriller and suspense movie",Actors=actors3,Producer=new Producer(){Name="aamir khan",Dob=new DateTime(1967,12,12)} },
                new Movie(){Name="welcome",ReleaseDate=new DateTime(1967,12,12),Plot="best comedy movie",Actors=actors4,Producer=new Producer(){Name="akshay",Dob=new DateTime(1968,12,11) } },
                new Movie(){Name="bhuj",ReleaseDate=new DateTime(1967,12,12),Plot="patriotic and action movie",Actors=actors5,Producer=new Producer(){Name="ajay",Dob=new DateTime(1967,12,12)} },
                new Movie(){Name="kabir shingh",ReleaseDate=new DateTime(1967,12,12),Plot="love and romantic movie",Actors=actors6,Producer=new Producer(){Name="shahid kapoor",Dob=new DateTime(1977,12,5) } },
                new Movie(){Name="mission mangal",ReleaseDate=new DateTime(1967,12,12),Plot="mars orbitor mission",Actors=actors7,Producer=new Producer(){Name="akshay",Dob=new DateTime(1968,12,11) } }

            };
            var a = movies.GroupBy((s) => s.ReleaseDate);
            foreach(var s in a)
            {
                Console.WriteLine(s.Key);
                foreach(var p in s)
                {
                    Console.WriteLine(p.Name);
                }
            }
            
        }
    }
    public class ExceptionHandle:Exception
    {
        string _exceptionMessage;
        public ExceptionHandle(string s)
        {
            _exceptionMessage = s;
        }
        public void PrintException()
        {
            Console.WriteLine(_exceptionMessage);
        }

    }
    public class IMDB_AppServices
    {
        private readonly ActorsRepository _actorsRepository;
        private readonly ProducersRepository _producersRepository;
        private readonly MoviesRepository _moviesRepository;

        public IMDB_AppServices()
        {
            _actorsRepository = new ActorsRepository();
            _producersRepository = new ProducersRepository();
            _moviesRepository = new MoviesRepository();
        }

        public void AddActor(string name, string dob)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(dob))
            {
                throw new ExceptionHandle("Name and date can't be empty!");
                
            }
            DateTime dateValue;
            if(!DateTime.TryParse(dob, out dateValue))
            {
                throw new ExceptionHandle("you entered wrong date type");
               
            }
            //dateValue = Convert.ToDateTime(dateValue);
            var actor = new Actor(name, dateValue);

            _actorsRepository.Add(actor);
        }
        public List<Actor> GetActors()
        {
            return _actorsRepository.Get();
        }
        public void AddProducer(string name, string dob)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(dob.ToString()))
            {
                throw new ExceptionHandle("Name and date can't be empty!");
                //Console.WriteLine("Invalid arguments");
                //return;
            }
            DateTime dateValue;
            if (!DateTime.TryParse(dob, out dateValue))
            {
                throw new ExceptionHandle("you entered wrong date type");
                //Console.WriteLine("you entered wrong type date time");
                //return;
            }
            //dateValue = Convert.ToDateTime(dateValue);
            var producer = new Producer(name, dateValue);

            _producersRepository.Add(producer);
        }
        public List<Producer> GetProducers()
        {
            return _producersRepository.Get();
        }

        public void AddMovies(string name, string releaseDate, string plot, string actorsIndex, string producerIndex)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(releaseDate) || string.IsNullOrEmpty(actorsIndex)||string.IsNullOrEmpty(producerIndex))
            {
                throw new ExceptionHandle("Name , Release date , chosen actor and chosen producer can't be empty !");
                //Console.WriteLine("Invalid arguments");
                //return;
            }
            string[] arr = actorsIndex.Split(' ');
            if (arr.Length == 0)
            {
                throw new ExceptionHandle("You have entered wrong date type");
                //Console.WriteLine("you forget to select actors please recontinue");
                //return;
                
            }

            List<string> selectedActors = new List<string>(arr);
            
            List<Actor> selectedActorList = ReturnSelectedActors(selectedActors);

            Producer selectedProducer = ReturnSelectedProducer(producerIndex);

            DateTime dateValue;
            if (!DateTime.TryParse(releaseDate, out dateValue))
            {
                throw new ExceptionHandle("You have entered wrong date type");
                
            }
           
            var movie = new Movie(name, dateValue, plot, selectedActorList, selectedProducer);

            _moviesRepository.Add(movie);
            //_moviesRepository.Add(movie);
        }
        public List<Movie> GetMovies()
        {
            return _moviesRepository.Get();
        }
        public void SelectedMoviesToDelete(string selectedMovies)
        {
            if (string.IsNullOrEmpty(selectedMovies))
            {
                throw new ExceptionHandle("can't be empty");

            }
            string[] arr2 = selectedMovies.Split(' ');
            if (arr2 == null || arr2.Length == 0)
            {
                throw new ExceptionHandle("you have selected wrong type indexes");

            }
            List<string> l = new List<string>(arr2);
            List<Movie> selectedMoviesToDelete = ReturnSelectedMovies(l);

            List<Movie> moviesList = GetMovies();

            foreach (var index in selectedMoviesToDelete)
            {
                moviesList.Remove(index);
            }
        }
        
        public void PrintActors()
        {
            List<Actor> actors = GetActors();
            for (int i = 0; i < actors.Count; i++)
            {
                Console.Write((i + 1) + ". " + actors[i].Name + " ");

            }

        }
        public void PrintProducers()
        {
            List<Producer> producers = GetProducers();
            for (int i = 0; i < producers.Count; i++)
            {
                Console.Write((i + 1) + ". " + producers[i].Name + " ");

            }

        }
        public void PrintMovies()
        {

            List<Movie> movies = GetMovies();
            for (int i = 0; i < movies.Count; i++)
            {
                Console.WriteLine((i + 1) + ". movies name : " + movies[i].Name + " ( " + movies[i].ReleaseDate.Year + " )");
                Console.WriteLine("plot : " + movies[i].Plot);
                List<Actor> actorList = movies[i].Actors;
                Console.Write("Actors - ");
                foreach (var obj in actorList)
                {
                    Console.Write(obj.Name + " ");
                }
                Console.WriteLine();
                Console.WriteLine("Producer - " + movies[i].Producer.Name);
            }

        }
        
        
        public List<Actor> ReturnSelectedActors(List<string> l)
        {
            List<Actor> actorList = GetActors();
            foreach (var index in l)
            {
                int idx;
                bool a = int.TryParse(index, out idx);
                if (!a|| idx > actorList.Count || idx <= 0)
                    throw new ExceptionHandle("You have entered wrong index type or wrong indexes");
                
            }
            List<int> selectedIndexes = new List<int>();
            foreach (var index in l)
            {
                int i = int.Parse(index);
                selectedIndexes.Add(i);
            }
            selectedIndexes.Sort();
            List<Actor> ans = new List<Actor>();
            foreach (var index in selectedIndexes)
            {  
                Actor act = actorList[index- 1];
                ans.Add(act);
            }
            return ans;
        }
        public Producer ReturnSelectedProducer(string index)
        {
            List<Producer> producerList = GetProducers();
            int idx;
            bool a = int.TryParse(index, out idx);
            if (!a || idx > producerList.Count || idx <= 0)
                throw new ExceptionHandle("You have entered wrong index type or wrong indexes");

            return producerList[idx - 1];
        }
        public List<Movie> ReturnSelectedMovies(List<string> selectedMovies)
        {
            List<Movie> movieList = GetMovies();
            foreach (var index in selectedMovies)
            {
                int idx;
                bool a = int.TryParse(index, out idx);
                if (!a || idx > movieList.Count || idx <= 0)
                    throw new ExceptionHandle("You have entered wrong index type or wrong indexes");

            }
            List<int> selectedIndexes = new List<int>();
            foreach (var index in selectedMovies)
            {
                int i = int.Parse(index);
                selectedIndexes.Add(i);
            }
            selectedIndexes.Sort();
            List<Movie> ans = new List<Movie>();
            foreach (var index in selectedIndexes)
            {
                Movie movie = movieList[index - 1];
                ans.Add(movie);
            }
            return ans;

        }
        public bool IsActorListEmpty()
        {
            var actorList = GetActors();
            if (actorList == null || actorList.Count == 0)
                return true;
            return false;
        }
        public bool IsProducerListEmpty()
        {
            var producerList = GetProducers();
            if (producerList == null || producerList.Count == 0)
                return true;
            return false;
        }
        public bool IsMoviesListEmpty()
        {
            var movieList = GetMovies();
            if (movieList == null || movieList.Count == 0)
                return true;
            return false;
        }
      
    }

}
