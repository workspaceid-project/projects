﻿Feature: SpecFlowFeature2
	Simple calculator for adding two numbers

@addMovies
Scenario: add Movie
	When user enter movie name 'radhe'
	And user enter release date '12/5/2021'
	And user enter plot 'action movie'
	And user enter list of actors '1 2 3'
	And user enter producer '1'
	And add movie into list
	Then added movie look like this
	| Name  | ReleaseDate | Plot         |
	| radhe | 12/05/2021  | action movie |
	And actor list
	| Name   | Dob        |
	| aamir  | 12/07/1967 |
	| salman | 12/07/1967 |
	| akshay | 12/07/1967 |
	And producer list
	| Name    | Dob        |
	| salman | 12/07/1967 |

@listMovies	
Scenario: List all movies in IMDB
	Given I have a list of movie
	When I fetch my movies
	Then I should have the following movie 
	| Name  | ReleaseDate | Plot         |
	| radhe | 12/05/2021  | action movie |
	And these are actors
	| Name   | Dob        |
	| aamir  | 12/07/1967 |
	| salman | 12/07/1967 |
	| akshay | 12/07/1967 |
	And these are producers
	| Name    | Dob        |
	| salman | 12/07/1967 |
