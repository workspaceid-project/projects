﻿using IMDB_App.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;


namespace IMDB_App.Test
{
    [Binding]
    public sealed class MovieStepDefinaion
    {
        

        IMDB_AppServices services = new IMDB_AppServices();
        string _movieName;
        string _releaseDate;
        string _plot;
        string _listOfActors;
        string _producer;
        ScenarioContext _context;
        public MovieStepDefinaion( ScenarioContext context)
        {
            _context = context;
        }
        [When(@"user enter movie name '(.*)'")]
        public void WhenUserEnterMovieName(string movieName)
        {
            _movieName = movieName;
        }

        [When(@"user enter release date '(.*)'")]
        public void WhenUserEnterReleaseDate(string movieReleaseDate)
        {
            _releaseDate = movieReleaseDate;
        }

        [When(@"user enter plot '(.*)'")]
        public void WhenUserEnterPlot(string moviePlot)
        {
            _plot = moviePlot;
        }

        [When(@"user enter list of actors '(.*)'")]
        public void WhenUserEnterListOfActors(string listOfActors)
        {
            _listOfActors = listOfActors;
        }


        [When(@"user enter producer '(.*)'")]
        public void WhenUserEnterProducer(string producer)
        {
            _producer = producer;
        }
        


        [When(@"add movie into list")]
        public void WhenAddMovieIntoList()
        {
            
             services.AddMovies(_movieName, _releaseDate, _plot, _listOfActors, _producer);
            
            
        }
        [Then(@"added movie look like this")]
        public void ThenAddedMovieLookLikeThis(Table table)
        {


            var movie = services.GetMovies();
            _context.Add("movie", movie);

            table.CompareToSet(movie);
        }
        [Then(@"actor list")]
        public void ThenActorList(Table table)
        {
            var movies = services.GetMovies();
            
            table.CompareToSet(movies[0].Actors);

        }

        

        [Then(@"producer list")]
        public void WhenProducerList(Table table)
        {
            var movies = services.GetMovies();
            table.CompareToInstance(movies[0].Producer);
        }


        /// <summary>
        /// //scenario implementation of list movie
        /// </summary>

        [Given(@"I have a list of movie")]
        public void GivenIHaveAListOfMovie()
        {
        }

        [When(@"I fetch my movies")]
        public void WhenIFetchMyMovies()
        {
            var movie = services.GetMovies().FirstOrDefault();
            _context.Add("movie", movie);
            
        }

        [Then(@"I should have the following movie")]
        public void ThenIShouldHaveTheFollowingMovie(Table table)
        {

            table.CompareToInstance(_context.Get<Movie>("movie"));
        }

        [Then(@"these are actors")]
        public void ThenTheseAreActors(Table table)
        {
            
            table.CompareToSet(_context.Get<Movie>("movie").Actors);
        }
        [Then(@"these are producers")]
        public void ThenTheseAreProducers(Table table)
        {
            table.CompareToInstance(_context.Get<Movie>("movie").Producer);
        }


        [BeforeScenario("addMovies")]
        public void movieDefine()
        {
            services.AddActor("aamir", "12/07/1967");
            services.AddActor("salman", "12/07/1967");
            services.AddActor("akshay", "12/07/1967");

            services.AddProducer("salman", "12/07/1967");
            


        }
        [BeforeScenario("listMovies")]
        public void ListMovies()
        {
            services.AddActor("aamir", "12/07/1967");
            services.AddActor("salman", "12/07/1967");
            services.AddActor("akshay", "12/07/1967");

            services.AddProducer("salman", "12/07/1967");
            services.AddMovies("radhe", "12/05/2021", "action movie", "1 2 3", "1");
        }



    }
}
