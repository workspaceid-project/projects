﻿using System;
using System.Collections.Generic;

namespace IMDB_App
{

    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("choose 1 to 5 and do operation\n1 List Movies\n2 Add Movie\n3 Add Actor\n4 Add Producer\n5 Delete Movie\n6 Exit\n");
            IMDB_AppServices services = new IMDB_AppServices();

            while (true)
            {
                try
                {

                    int selectedNumber = ReadInputFromConsole();
                    switch (selectedNumber)
                    {
                        case 1:
                            if (services.IsMoviesListEmpty())
                            {
                                Console.WriteLine("Movies List is Empty so Please First insert movies to display");
                                Console.WriteLine("---------------");
                                continue;
                            }

                            services.PrintMovies();
                            Console.WriteLine("---------------");

                            break;
                        case 2:
                            if (services.IsActorListEmpty() && services.IsProducerListEmpty())
                            {
                                Console.WriteLine("actor List and producer list are empty so please first insert actors and producer to insert movie");
                                Console.WriteLine("---------------");
                                continue;
                            }
                            else if (services.IsActorListEmpty())
                            {
                                Console.WriteLine("actor List is empty so please first insert actors to insert movie");
                                Console.WriteLine("---------------");
                                continue;

                            }
                            else if (services.IsProducerListEmpty())
                            {
                                Console.WriteLine("producer List is empty so please first insert producer to insert movie");
                                Console.WriteLine("---------------");
                                continue;

                            }

                            Console.WriteLine("Enter Movie Name : ");
                            string moviesName = Console.ReadLine();

                            Console.WriteLine("Release Date: in (dd/mm/yyyy)");
                            string releaseDate = Console.ReadLine();

                            Console.WriteLine("Enter Plot:");
                            string plot = Console.ReadLine();

                            Console.WriteLine("Choose Actor:");
                            services.PrintActors();
                            Console.WriteLine();
                            Console.WriteLine("select actors by its index and spaces between them");
                            string selectedActorInput = Console.ReadLine();


                            services.PrintProducers();
                            Console.WriteLine();
                            Console.WriteLine("select one producer by its index");
                            string selectedProducerInput = Console.ReadLine();

                            services.AddMovies(moviesName, releaseDate, plot, selectedActorInput, selectedProducerInput);
                            Console.WriteLine("---------------");



                            break;
                        case 3:
                            Console.WriteLine("Enter Actor Name:");
                            string actorName = Console.ReadLine();

                            Console.WriteLine("Enter Actor Dob: in (dd/mm/yyyy)");
                            string actorDob = Console.ReadLine();

                            services.AddActor(actorName, actorDob);
                            Console.WriteLine("---------------");

                            break;
                        case 4:

                            Console.WriteLine("Enter Producer Name:");
                            string producerName = Console.ReadLine();

                            Console.WriteLine("Producer Dob: in (dd/mm/yyyy)");
                            string producerDob = Console.ReadLine();

                            services.AddProducer(producerName, producerDob);
                            Console.WriteLine("---------------");

                            break;
                        case 5:
                            if (services.IsMoviesListEmpty())
                            {
                                Console.WriteLine("movies list is empty you can't delete operation");
                                Console.WriteLine("---------------");
                                continue;
                            }
                            services.PrintMovies();
                            Console.WriteLine("select movies by its index");
                            string deleteMovies = Console.ReadLine();

                            services.SelectedMoviesToDelete(deleteMovies);
                            Console.WriteLine("---------------");

                            break;
                        case 6:
                            Environment.Exit(0);
                            break;
                        default:
                            break;
                    }

                }
                catch (ExceptionHandle e)
                {
                    e.PrintException();
                    
                }

            }

        }

        public static int ReadInputFromConsole()
        {
            int number;
            Console.WriteLine("What do you want to do?");

            string input = Console.ReadLine();
            bool n = int.TryParse(input, out number);
            if (!n || number < 1 || number > 6)
            {
                throw new ExceptionHandle("You have entered wrong choice!\n  Please select one of them");

            }
            return number;
        }
    }
}
