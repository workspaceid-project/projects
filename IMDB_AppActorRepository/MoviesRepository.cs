﻿using IMDB_App.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMDB_App.Repository
{
    public class MoviesRepository
    {
        private readonly List<Movie> _movies;

        public MoviesRepository()
        {
            _movies = new List<Movie>();
            
        }

        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }

        public  List<Movie> Get()
        {
            return  _movies;
        }
        
    }
}
