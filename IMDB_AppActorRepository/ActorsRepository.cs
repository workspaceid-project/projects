﻿using IMDB_App.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMDB_App.Repository
{
    public class ActorsRepository
    {
        private readonly List<Actor> _actors;

        public ActorsRepository()
        {
            _actors = new List<Actor>();
        }

        public void Add(Actor actor)
        {
            _actors.Add(actor);
        }

        public List<Actor> Get()
        {
            return _actors;
        }
        
    }
}
