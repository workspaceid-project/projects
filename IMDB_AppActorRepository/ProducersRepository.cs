﻿using IMDB_App.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMDB_App.Repository
{
    public class ProducersRepository
    {
        private readonly List<Producer> _producers;

        public ProducersRepository()
        {
            _producers= new List<Producer>();
        }

        public void Add(Producer producer)
        {
            _producers.Add(producer);
        }

        public List<Producer> Get()
        {
            return _producers;
        }
        
    }
}
